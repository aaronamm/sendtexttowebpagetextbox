﻿namespace SendTextToWebPageTextbox
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.txtTextToSend = new System.Windows.Forms.TextBox();
            this.lblToSend = new System.Windows.Forms.Label();
            this.btnSend = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // webBrowser
            // 
            this.webBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser.Location = new System.Drawing.Point(2, 2);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(998, 430);
            this.webBrowser.TabIndex = 0;
            // 
            // txtTextToSend
            // 
            this.txtTextToSend.Location = new System.Drawing.Point(363, 455);
            this.txtTextToSend.Name = "txtTextToSend";
            this.txtTextToSend.Size = new System.Drawing.Size(100, 20);
            this.txtTextToSend.TabIndex = 1;
            // 
            // lblToSend
            // 
            this.lblToSend.AutoSize = true;
            this.lblToSend.Location = new System.Drawing.Point(254, 458);
            this.lblToSend.Name = "lblToSend";
            this.lblToSend.Size = new System.Drawing.Size(66, 13);
            this.lblToSend.TabIndex = 2;
            this.lblToSend.Text = "Text to send";
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(512, 452);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 3;
            this.btnSend.Text = "Send (Enter)";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // MainForm
            // 
            this.AcceptButton = this.btnSend;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1001, 487);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.lblToSend);
            this.Controls.Add(this.txtTextToSend);
            this.Controls.Add(this.webBrowser);
            this.Name = "MainForm";
            this.Text = "Send text to web page inbox";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WebBrowser webBrowser;
        private System.Windows.Forms.TextBox txtTextToSend;
        private System.Windows.Forms.Label lblToSend;
        private System.Windows.Forms.Button btnSend;
    }
}

